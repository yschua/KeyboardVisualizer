#include "Animation.h"
#include "AnimationArgs.h"

namespace LightShow
{

void StaticAnimation::Start(const AnimationArgs& args)
{
    color = args.color;
}

void StaticAnimation::Draw(RGBColor (*pixels)[PIXEL_ROW_LENGTH])
{
    for (int i = 0; i < PIXEL_ROW_LENGTH; i++)
    {
        (*pixels)[i] = color;
    }
}

}
