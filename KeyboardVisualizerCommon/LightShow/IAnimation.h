#pragma once

using RGBColor = unsigned int;

namespace LightShow
{

struct AnimationArgs;

class IAnimation
{
protected:
    static constexpr int PIXEL_ROW_LENGTH{256};

public:
    virtual ~IAnimation() = default;
    virtual void Start(const AnimationArgs& args) = 0;
    virtual void Draw(RGBColor (*pixels)[PIXEL_ROW_LENGTH]) = 0;
};

}
