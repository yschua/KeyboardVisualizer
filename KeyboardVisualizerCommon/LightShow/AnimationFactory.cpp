#include "AnimationFactory.h"
#include "Animation.h"

#include <stdexcept>

namespace LightShow
{

std::unique_ptr<IAnimation> AnimationFactory::Create(AnimationType type)
{
    switch (type)
    {
    case AnimationType::Static:
        return std::make_unique<StaticAnimation>();
    default:
        throw std::invalid_argument("Undefined AnimationType");
    }
}

}