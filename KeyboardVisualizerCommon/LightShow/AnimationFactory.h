#include "AnimationType.h"

#include <memory>

namespace LightShow
{

class IAnimation;

class AnimationFactory
{
public:
    static std::unique_ptr<IAnimation> Create(AnimationType type);
};

}
