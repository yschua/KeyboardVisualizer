#pragma once

#include "AnimationType.h"

using RGBColor = unsigned int;

namespace LightShow
{

struct AnimationArgs
{
    AnimationType type;
    int frames;
    RGBColor color;
};

}