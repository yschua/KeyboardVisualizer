#pragma once

#include "IAnimation.h"

namespace LightShow
{

class StaticAnimation : public virtual IAnimation
{
public:
    void Start(const AnimationArgs& args) override;
    void Draw(RGBColor (*pixels)[PIXEL_ROW_LENGTH]) override;

private:
    RGBColor color;
};

}